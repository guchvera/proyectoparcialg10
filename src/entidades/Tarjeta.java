/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidades;

import interfaz.Data;
import interfaz.Util;

/**
 *
 * @author Brank
 */
public class Tarjeta {

    private String nombre;
    private String tipo;

    public Tarjeta(String nombre, String tipo) {
        this.nombre = nombre;
        this.tipo = tipo;
    }
/**
 * Metodo para agregar una tarjeta
 * @return tarjeta Retorna una tarjeta 
 **/
    public static Tarjeta nuevaTarjeta() {
        System.out.print("Ingrese el nombre de la tarjeta : ");
        String nombreTarjeta = Util.ingresoString().toUpperCase();
        String tipoTarjeta = seleccionarTipoTarjeta();
        Tarjeta tarjeta = new Tarjeta(nombreTarjeta, tipoTarjeta);
        for (Tarjeta t: Data.tarjetas){
            if (t.equals(tarjeta)){
                tarjeta = t;
            }
        }
        return tarjeta;
    }
/**
 * Metodo para seleccionar el tipo de una tarjeta
 * @return String retorna un tipo de tarjeta
 **/
    public static String seleccionarTipoTarjeta() {
        System.out.println("Elija el tipo de tarjeta : ");
        System.out.println("1. Crédito\n2. Débito\n3. Afiliación");
        boolean tarjetaCorrecta = false;
        String tipoTarjeta = Util.ingresoString();
        while (!(tarjetaCorrecta)) {
            switch (tipoTarjeta) {
                case "1":
                    tipoTarjeta = "Crédito";
                    tarjetaCorrecta = true;
                    break;
                case "2":
                    tipoTarjeta = "Débito";
                    tarjetaCorrecta = true;
                    break;
                case "3":
                    tipoTarjeta = "Afiliación";
                    tarjetaCorrecta = true;
                    break;
                default:
                    System.out.print("Opción incorrecta. Elija el tipo de tarjeta : ");
                    tipoTarjeta = Util.ingresoString();
            }
        }
        return tipoTarjeta;
    }

    /* GETTERS & SETTERS */
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @Override
    public String toString() {
        return nombre + ", " + tipo;
    }

    @Override
    public boolean equals(Object obj) {
        boolean retorno = false;
        if (obj instanceof Tarjeta) {
            if ((((Tarjeta) obj).getNombre().equals(this.nombre)) && (((Tarjeta) obj).getNombre().equals(this.tipo))) {
                retorno = true;
            }
        }
        return retorno;
    }
}
