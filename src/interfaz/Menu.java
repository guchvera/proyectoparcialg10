/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaz;

import java.util.ArrayList;

/**
 *
 * @author Brank
 */
public abstract class Menu {

    ArrayList<String> opciones;

    public Menu(ArrayList<String> opciones) {
        this.opciones = opciones;
    }

    public void cargarMenu() {
    }

    public void mostrarMenu() {
        Util.limpiarPantalla();        
        System.out.println("================================================================================");
        System.out.println();       
        for (Integer i = 0; i < opciones.size(); i += 1) {
            System.out.println(i + 1 + ". " + opciones.get(i));
        }
        System.out.println();
        System.out.println("================================================================================");
    }    
}
