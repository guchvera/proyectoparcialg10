/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaz;

import java.util.*;
import entidades.*;

/**
 *
 * @author Brank
 */
public class MenuPromociones extends Menu {

    public MenuPromociones(ArrayList<String> opciones) {
        super(opciones);
    }
/**
 * Se sobreescribe el metodo cargarMenu() de la clase padre Menu
 **/
    @Override
    public void cargarMenu() {
        boolean regresar = false;
        while (!(regresar)) {
            mostrarMenu();
            System.out.print("Ingrese opción : ");
            String opcion = Util.ingresoString();
            while ((!(Util.isNumeric(opcion))) || (!(Util.isBetween(1, 5, opcion)))) {
                System.out.print("Opción incorrecta. Ingrese nuevamente : ");
                opcion = Util.ingresoString();
            }
            switch (opcion) {
                case "1":
                    System.out.println();
                    System.out.println("*-----------------------------------------------------------------------------*");
                    Data.mostrarPromociones();
                    Util.continuar();
                    break;
                case "2":
                    System.out.print("Ingrese la descripcion de la promoción : ");
                    String nDescripcion = Util.ingresoString();
                    ArrayList<Promocion> promocionesDescripcion = Promocion.comprobarPromocionesDescripcion(nDescripcion);
                    if (!(promocionesDescripcion.isEmpty())) {
                        System.out.println();
                        System.out.println("*-----------------------------------------------------------------------------*");
                        for (Promocion p : promocionesDescripcion) {
                            System.out.println();
                            System.out.println("Empresa : " + p.getEstablecimiento().getEmpresa().getNombre());
                            System.out.println("Establecimiento : " + p.getEstablecimiento().getDireccion());
                            System.out.println(p);
                            System.out.println();
                            System.out.println("*-----------------------------------------------------------------------------*");
                        }
                        Util.continuar();
                    } else {
                        System.out.println();
                        System.out.println("No se encontraron resultados");
                        Util.continuar();
                    }
                    break;
                case "3":
                    System.out.println();
                    ArrayList<Dia> nDias = Promocion.seleccionarDiasValidez();
                    ArrayList<Promocion> promocionesDias = Promocion.comprobarPromocionesDias(nDias);
                    if (!(promocionesDias.isEmpty())) {
                        System.out.println();
                        System.out.println("*-----------------------------------------------------------------------------*");
                        for (Promocion p : promocionesDias) {
                            System.out.println();
                            System.out.println("Empresa : " + p.getEstablecimiento().getEmpresa().getNombre());
                            System.out.println("Establecimiento : " + p.getEstablecimiento().getDireccion());
                            System.out.println(p);
                            System.out.println();
                            System.out.println("*-----------------------------------------------------------------------------*");
                        }
                        Util.continuar();
                    } else {
                        System.out.println();
                        System.out.println("No se encontraron resultados");
                        Util.continuar();
                    }
                    break;
                case "4":
                    String nTipoTarjeta = Tarjeta.seleccionarTipoTarjeta();
                    ArrayList<Promocion> promocionesTarjeta = Promocion.comprobarPromocionesTarjeta(nTipoTarjeta);
                    if (!(promocionesTarjeta.isEmpty())) {
                        System.out.println();
                        System.out.println("*-----------------------------------------------------------------------------*");
                        for (Promocion p : promocionesTarjeta) {
                            System.out.println();
                            System.out.println("Empresa : " + p.getEstablecimiento().getEmpresa().getNombre());
                            System.out.println("Establecimiento : " + p.getEstablecimiento().getDireccion());
                            System.out.println(p);
                            System.out.println();
                            System.out.println("*-----------------------------------------------------------------------------*");
                        }
                        Util.continuar();
                    } else {
                        System.out.println();
                        System.out.println("No se encontraron resultados");
                        Util.continuar();
                    }
                    break;
                case "5":
                    regresar = true;
                    break;
            }
        }
    }
/**
 * Metodo añadir opciones al menu de promociones
 * @return Arraylist Retorna las opciones del menu de promociones
 **/
    public static ArrayList<String> añadirOpciones() {
        ArrayList<String> opciones = new ArrayList<>();
        opciones.add("Todos");
        opciones.add("Por descripción");
        opciones.add("Por días de validez");
        opciones.add("Por tarjeta");
        opciones.add("Regresar al menú principal");
        return opciones;
    }
}
